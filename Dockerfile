FROM cgr.dev/chainguard/wolfi-base:latest

WORKDIR /app

RUN apk update && apk add --no-cache \
    nodejs \
    npm

COPY . .

RUN npm ci

EXPOSE 5173

ENTRYPOINT ["npm", "run", "dev"]
